﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MapPhone.Resources;
using System.Xml;
using System.Threading.Tasks;
using System.Net;
using Windows.Data.Xml.Dom;
using System.Xml.Linq;
using Windows.Data.Json;
using System.Device.Location;
using Microsoft.Phone.Maps.Controls;
using System.Windows.Media;

namespace MapPhone
{
    public partial class MainPage : PhoneApplicationPage
    {

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            
        }

        private void parseoXml(object sender, DownloadStringCompletedEventArgs e)
        {

            List<GeoCoordinate> listPuntos = new List<GeoCoordinate>();

            if (e.Error == null)
            {
                XDocument xdoc = XDocument.Parse(e.Result, LoadOptions.None);

                IEnumerable<XElement> Steps = xdoc.Descendants(XName.Get("step"));
                foreach (XElement Step in Steps)
                {
                    IEnumerable<XElement> Locations = Step.Descendants(XName.Get("start_location"));
                    foreach(XElement Location in Locations)
                    {
                        GeoCoordinate location = new GeoCoordinate();
                        location.Latitude = Double.Parse(Location.Descendants(XName.Get("lat")).FirstOrDefault().Value);
                        location.Longitude = Double.Parse(Location.Descendants(XName.Get("lng")).FirstOrDefault().Value);
                        listPuntos.Add(location);
                    }

                }
                if(listPuntos != null && listPuntos.Count != 0)
                {
                    dibujarRuta(listPuntos);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Destino no encontrado.");
                }
            }
            
        }

        private void dibujarRuta(List<GeoCoordinate> listPuntos)
        {
            MapPolyline line = new MapPolyline();
            line.StrokeColor = Colors.Red;
            line.StrokeThickness = 5;
            foreach (GeoCoordinate coordenadaLine in listPuntos)
            {
                line.Path.Add(coordenadaLine);
            }
            mapa.MapElements.Add(line);
            //Aumentamos el zoom
            mapa.ZoomLevel = 8;
            //Centramos el mapa en el inicio de la ruta
           mapa.Center = listPuntos.First();
            
        }
        private void btnCalcularRuta_Click(object sender, RoutedEventArgs e)
        {

               String destino = txtDestino.Text;

               if(destino != null || !destino.Equals("")) { 

                   WebClient client = new WebClient();
                   client.DownloadStringAsync(new Uri("https://maps.googleapis.com/maps/api/directions/xml?origin=Madrid&destination="+ destino + "&departure_time=1343641500&mode=car"));
                   client.DownloadStringCompleted += parseoXml;

               } 

           
        }
    }
}